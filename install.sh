#!/bin/bash

# Check if the user has root permissions to install the CLI
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Check if the CLI is already installed
if command -v emailnator &> /dev/null; then
    echo "The CLI is already installed"
    exit 0
fi

# Install the CLI using npm
npm install
npm link
npm install -g

# Verify if the installation was successful
if command -v emailnator &> /dev/null; then
    echo "The installation of the CLI was successful"
    exit 0
else
    echo "The installation of the CLI failed"
    exit 1
fi
