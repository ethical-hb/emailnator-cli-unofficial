#!/usr/bin/env node

import { Command } from 'commander';
import { generateEmail } from './bin/generate';
import { listEmailsToDatabase } from './bin/list';
import { inbox } from './bin/inbox';
import { message } from './bin/message';

const program = new Command();
program.name('emailnator');
program.version('1.0.0');
program.description('A CLI to generate email addresses and read emails');

console.log("Emailnator Unofficial CLI " + program.version() + "\n")

program
    .command('generate-email')
    .alias('ge')
    .description('Generate a new email address')
    .action(() => {
        generateEmail()
            .then((email) => {
                console.log(email);
            })
            .catch((err) => {
                console.error(err.message);
            });
    });

program
    .command('generate-email-multi <number>')
    .alias('gem')
    .description('Generate multiple email addresses')
    .action((number: string) => {
        const num = parseInt(number, 10);
        if (isNaN(num)) {
            console.error('Invalid number');
            return;
        }
        const promises = [];
        for (let i = 0; i < num; i++) {
            promises.push(generateEmail());
        }
        Promise.all(promises)
            .then((emails) => {
                console.log(emails.join('\n'));
            })
            .catch((err) => {
                console.error(err.message);
            });
    });

program
    .command('list')
    .alias('l')
    .description('List all email addresses generated')
    .action(() => {
        listEmailsToDatabase()
            .then((value: unknown) => {
                const emails = value as any[];
                const emailList = emails.map((email: any) => email.email);
                console.log(emailList.join('\n'));
            })
            .catch((err: Error) => {
                console.error(err.message);
            });
    });

program
    .command('inbox <email>')
    .alias('i')
    .description('Show the inbox for a given email address')
    .action((email: string) => {
        inbox(email)
            .then((value: any) => {
                console.log("inbox to " + email)
                
                const messages = value.map((message: any[]) => {
                    if (!message[3].startsWith("/inbox/")) {
                        return null;
                    }
                    return {
                        from: message[0],
                        object: message[1],
                        time: message[2],
                        link: "https://emailnator.com/" + message[3],
                        id: message[3].substring(message[3].indexOf("@gmail.com/") + 11, message[3].length)
                    };
                });

                // si il y a null dans le tableau, on le supprime
                for (let i = 0; i < messages.length; i++) {
                    if (messages[i] === null) {
                        messages.splice(i, 1);
                    }
                }
                
                console.log(messages)
            })
            .catch((err: Error) => {
                console.error(err.message);
            });
    });
    

    
program
    .command('message <email> <id>')
    .alias('m')
    .description('show the message for a given email address and id')
    .action((email: string, id: string) => {
        message(id, email)
            .then((value: any) => {
                const html = value.join('');
                console.log(html);
            })
            .catch((err: Error) => {
                console.error(err.message);
            });
    });

program.parse(process.argv);