import * as sqlite3 from 'sqlite3';

/**
 * list emails from database
 * @returns all emails saved in database
 */
export function listEmailsToDatabase() {
    return new Promise((resolve, reject) => {
        const db = new sqlite3.Database("emails.db");
        db.all("SELECT * FROM emails", function (err, rows) {
            if (err) {
                console.error(err.message);
                reject(err);
            } else {
                resolve(rows);
            }
        });
        db.close();
    });
}