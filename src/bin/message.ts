import puppeteer from "puppeteer";

/**
 * get html message content
 * @param id 
 * @param email 
 * @returns html content
 */
export async function message(id: string, email: string) {
    const browser = await puppeteer.launch({ headless: "new" });
    const page = await browser.newPage();

    await page.setCacheEnabled(false);
    await page.setOfflineMode(false);
    await page.setBypassServiceWorker(true);

    await page.setRequestInterception(true);
    page.on("request", (request) => {
        if (
            ["image", "stylesheet", "font", "medias"].indexOf(
                request.resourceType()
            ) !== -1
        ) {
            request.abort();
        } else {
            request.continue();
        }
    });

    page.on("dialog", async (dialog) => {
        await dialog.dismiss();
    });

    await page.evaluateOnNewDocument(() => {
        Object.defineProperty(navigator, "webdriver", {
            get: () => false,
        });
    });

    await page.goto("https://www.emailnator.com/inbox/" + email + "/" + id);

    await page.waitForSelector(".fc-button-background");
    await page.click(".fc-button-background");

    const mailElements = await page.$$eval(
        '#root > div > section > div > div > div.mb-3.col-lg-6.col-sm-12 > div > div > div.card > div > div',
        (elements) => {
            return elements.map((element) => {
                const content = element.innerHTML;

                return content;
            });
        }
    );

    await page.close();
    await browser.close();
    return mailElements;
}
