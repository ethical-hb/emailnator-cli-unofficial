# Emailnator Unofficial CLI

This CLI is designed to generate email addresses and read emails from the emailnator.com website. Please note that this project is for **lucrative purposes only** and I am not responsible for any misuse of this tool.

This project involves reverse engineering and web scraping of emailnator.com, which is considered ethical hacking. Please use this tool responsibly and in accordance with all applicable laws and regulations.

## Installation

To install the CLI, run the following command

```bash
cd emailnator-cli-unofficial
sudo npm install -g
emailnator --help
```

Or use the install.sh script

```bash
cd emailnator-cli-unofficial
sudo ./install.sh
emailnator --help
```

## Database

This code indicates that the email database is stored in a SQLite3 database file named "emails.db" which is located in the same directory as the CLI.

## Usage

To use the CLI, run one of the following commands:

- `emailnator generate-email`: Generates a new email address.
- `emailnator generate-email-multi <number>`: Generates multiple email addresses.
- `emailnator list`: Lists all email addresses generated.
- `emailnator inbox <email>`: Shows the inbox for a given email address.
- `emailnator message <email> <id>`: Shows the message for a given email address and ID.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
